***Tech specs***
================

* OS: Debian Stretch Slim
* Services:
	* supervisord
		* nginx (1.14)
		* php-fpm (7.4)
* PHP extensions:
	* php7.4-common
	* php7.4-fpm
	* php7.4-xml
	* php7.4-pgsql
	* php7.4-mysql
	* php7.4-sqlite3
	* php7.4-gd
	* php7.4-intl
	* php7.4-phpdbg
	* php7.4-tidy
	* php7.4-mbstring
	* php7.4-zip
	* php7.4-soap
	* php7.4-sybase
	* php7.4-xsl
	* php7.4-curl
	* php-xdebug
* Some useful installed packages:
	* wget
	* curl 
	* sudo 
	* git
	* htop 
	* nano 
	* zsh (and oh-my-zsh)
	* zip 
	* unzip 
	* net-tools 
	* iputils-ping 
	* composer
	* mysql-client
	* postgresql-client-12
	* nodejs

***How to config your own image***
==================================
by config files from outside in ***`assets/configs/[nginx|php-fpm|supervisor]`***

* ***`nginx:`*** you can change all nginx configuration in ***`default`*** file and build your own image
* ***`php-fpm:`*** you can override all of parameters in php.ini by editing ***`php-ini-overrides.ini`***
* ***`supervisor:`*** you can change ***`php-fpm's`*** and ***`nginx's`*** service in ***`supervisor-php-fpm.conf`*** 
and ***`supervisor-nginx.conf`***

***Conclusion***
===============
	
after you set all of your configurations, you can use ***`build`*** command like below:

	$ make build REGISTRY_NAME=registry.gitlab.com/[user]/task-list \
	DISTRO=debian \
	PROJECT_NAME=task-list \
	VERSION=v1-dev \
	DEBUG=true \
	APT_CACHER_SERVER=http://172.17.0.1:3142

another example:

	$ make build REGISTRY_NAME=registry.gitlab.com/[user]/task-list VERSION=v1 \
	DISTRO=debian \
	PROJECT_NAME=task-list 
	
and in the end, you can use default image (with default configs) of this project (always master is latest version):

	$ docker pull registry.gitlab.com/m3hr24d/docker-symfony:[master|vX.Y.Z]